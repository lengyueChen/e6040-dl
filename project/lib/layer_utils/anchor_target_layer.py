# --------------------------------------------------------
# Faster R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick, Xinlei Chen and Yiran Shi
# --------------------------------------------------------
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
from model.config import cfg
import numpy as np
import numpy.random as npr
from utils.cython_bbox import bbox_overlaps
from model.bbox_transform import bbox_transform

def anchor_target_layer(rpn_cls_score, gt_boxes, im_info, _feat_stride, all_anchors, num_anchors):
  """Same as the anchor target layer in original Fast/er RCNN """
  A = num_anchors
  total_anchors = all_anchors.shape[0]
  K = total_anchors / num_anchors
  # allow boxes to sit over the edge by a small amount
  border = 0
  height, width = rpn_cls_score.shape[1:3]
  #no boarder over edge in my implementation so it is 0
  inds_inside = np.where(
    (all_anchors[:, 0] >= border) &
    (all_anchors[:, 1] >= border) &
    (all_anchors[:, 2] < im_info[1]) &  
    (all_anchors[:, 3] < im_info[0])  
  )[0]
  labels = np.empty((len(inds_inside),), dtype=np.float32)
  labels.fill(-1)
  overlaps = bbox_overlaps(np.ascontiguousarray(all_anchors[inds_inside, :], dtype=np.float),np.ascontiguousarray(gt_boxes, dtype=np.float))
  if not cfg.TRAIN.RPN_CLOBBER_POSITIVES:    
    labels[overlaps[np.arange(len(inds_inside)), overlaps.argmax(axis=1)] < cfg.TRAIN.RPN_NEGATIVE_OVERLAP] = 0
  
  labels[np.where(overlaps == overlaps[overlaps.argmax(axis=0),np.arange(overlaps.shape[1])])[0]] = 1
  labels[overlaps[np.arange(len(inds_inside)), overlaps.argmax(axis=1)] >= cfg.TRAIN.RPN_POSITIVE_OVERLAP] = 1

  fg_n = int(cfg.TRAIN.RPN_FG_FRACTION * cfg.TRAIN.RPN_BATCHSIZE)
  if len(np.where(labels == 1)[0]) > fg_n:
    disable_inds = npr.choice(
      np.where(labels == 1)[0], size=(len(np.where(labels == 1)[0]) - fg_n), replace=False)
    labels[disable_inds] = -1

  background_n = cfg.TRAIN.RPN_BATCHSIZE - np.sum(labels == 1)
  if len(np.where(labels == 0)[0]) > background_n:
    disable_inds = npr.choice(
      np.where(labels == 0)[0], size=(len(np.where(labels == 0)[0]) - background_n), replace=False)
    labels[disable_inds] = -1
  box_t = bbox_transform(all_anchors[inds_inside, :], gt_boxes[overlaps.argmax(axis=1), :][:, :4]).astype(np.float32, copy=False)
  biw = np.zeros((len(inds_inside), 4), dtype=np.float32)
  # only the positive ones have regression targets
  biw[labels == 1, :] = np.array(cfg.TRAIN.RPN_BBOX_INSIDE_WEIGHTS)

  bow = np.zeros((len(inds_inside), 4), dtype=np.float32)
  if cfg.TRAIN.RPN_POSITIVE_WEIGHT < 0:
    bow[labels == 1, :] = np.ones((1, 4)) * 1.0 / np.sum(labels >= 0)
    bow[labels == 0, :] = np.ones((1, 4)) * 1.0 / np.sum(labels >= 0)
  else:
    bow[labels == 1, :] = (cfg.TRAIN.RPN_POSITIVE_WEIGHT /np.sum(labels == 1))
    bow[labels == 0, :] = ((1.0 - cfg.TRAIN.RPN_POSITIVE_WEIGHT) /np.sum(labels == 0))
  # map up to original set of anchors
  labels = _unmap(labels, total_anchors, inds_inside, fill=-1)
  box_t = _unmap(box_t, total_anchors, inds_inside, fill=0)
  biw = _unmap(biw, total_anchors, inds_inside, fill=0)
  bow = _unmap(bow, total_anchors, inds_inside, fill=0)
  # labels
  labels = (labels.reshape((1, height, width, A)).transpose(0, 3, 1, 2)).reshape((1, 1, A * height, width))
  # bbox_targets
  box_t = box_t .reshape((1, height, width, A * 4))
  # bbox_inside_weights
  biw = biw .reshape((1, height, width, A * 4))
  # bbox_outside_weights
  bow = bow.reshape((1, height, width, A * 4))
  return labels, box_t, biw, bow


def _unmap(data, count, inds, fill=0):
  """ Unmap a subset of item (data) back to the original set of items (of
  size count) """
  if len(data.shape) == 1:
    ret = np.empty((count,), dtype=np.float32)
    ret.fill(fill)
    ret[inds] = data
  else:
    ret = np.empty((count,) + data.shape[1:], dtype=np.float32)
    ret.fill(fill)
    ret[inds, :] = data
  return ret


