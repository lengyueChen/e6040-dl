# --------------------------------------------------------
# Faster R-CNN
# Licensed under The MIT License [see LICENSE for details]
# Written by Yiran Shi
# --------------------------------------------------------
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from model.config import cfg
from model.bbox_transform import bbox_transform_inv, clip_boxes, bbox_transform_inv_tf, clip_boxes_tf

import tensorflow as tf

def proposal_top_layer(rpn_cls_prob, rpn_bbox_pred, im_info, _feat_stride, anchors, num_anchors):
  """A layer that just selects the top region proposals
     without using non-maximal suppression,
     RPN implementation only supports a single input image.
  """

  s =(-1,4)
  t1, t2 = tf.nn.top_k(tf.reshape(rpn_cls_prob[:, :, :, num_anchors:], shape=(-1,)), k=cfg.TEST.RPN_TOP_N)
  RPN_proposals = tf.to_float(clip_boxes_tf(bbox_transform_inv_tf(tf.gather(anchors, t2), tf.gather(tf.reshape(rpn_bbox_pred, shape=s), t2))
                          , im_info[:2]))
  RPN_region = tf.concat([tf.zeros((cfg.TEST.RPN_TOP_N, 1)), RPN_proposals], 1)
  return RPN_region, tf.reshape(t1, shape=(-1, 1))
