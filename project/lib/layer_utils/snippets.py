# --------------------------------------------------------
# Tensorflow Faster R-CNN
# Licensed under The MIT License [see LICENSE for details]
# Written by Yiran shi
# --------------------------------------------------------
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import numpy as np
from layer_utils.generate_anchors import generate_anchors

def generate_anchors_pre(height, width, feat_stride=16, anchor_scales=(8, 16, 32), anchor_ratios=(0.5, 1, 2)):
  """Generate anchors to use in RPN"""

  p1 = np.array(anchor_ratios)
  p2 = np.array(anchor_scales)

  new_anchor = generate_anchors(base_size=16,ratios=p1, scales=p2)
  w,  h= tf.meshgrid(tf.range(width) * feat_stride , tf.range(height) * feat_stride)
  tranp = tf.transpose(tf.reshape(
    tf.transpose(tf.stack([tf.reshape(w, shape=(-1,)), 
    tf.reshape(h, shape=(-1,)), 
    tf.reshape(w, shape=(-1,)),
     tf.reshape(h, shape=(-1,))]))
  , shape=[1, tf.multiply(width, height), 4]), perm=(1, 0, 2))
  anchors_tf = tf.reshape(tf.add(tf.constant(new_anchor.reshape((1, new_anchor.shape[0], 4)), dtype=tf.int32), tranp), shape=(tf.multiply(width, height) * new_anchor.shape[0], 4))
  return tf.cast(anchors_tf, dtype=tf.float32), tf.multiply(width, height) * new_anchor.shape[0]

