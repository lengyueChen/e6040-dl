# --------------------------------------------------------
# Faster R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick, Sean Bell,Xinlei Chen and Yiran Shi
# --------------------------------------------------------
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import numpy.random as npr
import pdb
from model.config import cfg
from model.bbox_transform import bbox_transform
from utils.cython_bbox import bbox_overlaps


def proposal_target_layer(rpn_rois, rpn_scores, gt_boxes, _num_classes):
  """
  Assign object detection proposals to ground-truth targets. Produces proposal
  classification labels and bounding-box regression targets.
  """
  bbox_overlap, pred_score, box_region, rois_pre, transform = _sample_rois(
    rpn_rois, 
    np.vstack((rpn_scores, np.zeros((gt_boxes.shape[0], 1),
    dtype=gt_boxes.dtype))), 
    gt_boxes, 
    np.round(cfg.TRAIN.FG_FRACTION * cfg.TRAIN.BATCH_SIZE / 1),
    cfg.TRAIN.BATCH_SIZE / 1, _num_classes)
  return pred_score,box_region,bbox_overlap,rois_pre,transform,np.array(transform > 0).astype(np.float32)

def _sample_rois(all_rois, all_scores, gt_boxes, fg_rois_per_image, rois_per_image, num_classes):
  """Generate a random sample of RoIs comprising foreground and background
  examples.
  """
  p1 = np.ascontiguousarray(all_rois[:, 1:5], dtype=np.float)
  p2 = np.ascontiguousarray(gt_boxes[:, :4], dtype=np.float)
  overlaps = bbox_overlaps(p1,p2)
  labels = gt_boxes[overlaps.argmax(axis=1), 4]

  # Select foreground RoIs as those with >= FG_THRESH overlap
  fg_inds = np.where( overlaps.max(axis=1) >= cfg.TRAIN.FG_THRESH)[0]
  # Select background RoIs as those within [BG_THRESH_LO, BG_THRESH_HI)
  bg_inds = np.where(( overlaps.max(axis=1) < cfg.TRAIN.BG_THRESH_HI) &
                     ( overlaps.max(axis=1) >= cfg.TRAIN.BG_THRESH_LO))[0]

  if fg_inds.size > 0 and bg_inds.size > 0:
    fg_rois_per_image = min(fg_rois_per_image, fg_inds.size)
    fg_inds = npr.choice(fg_inds, size=int(fg_rois_per_image), replace=False)
    bg_rois_per_image = rois_per_image - fg_rois_per_image
    to_replace = bg_inds.size < bg_rois_per_image
    bg_inds = npr.choice(bg_inds, size=int(bg_rois_per_image), replace=to_replace)
  elif bg_inds.size > 0:
    to_replace = bg_inds.size < rois_per_image
    bg_inds = npr.choice(bg_inds, size=int(rois_per_image), replace=to_replace)
    fg_rois_per_image = 0
  elif fg_inds.size > 0:
    to_replace = fg_inds.size < rois_per_image
    fg_inds = npr.choice(fg_inds, size=int(rois_per_image), replace=to_replace)
    fg_rois_per_image = rois_per_image
  else:
    pdb.set_trace()
  # Select sampled values from various arrays:
  labels = labels[np.append(fg_inds, bg_inds)]
  temp = overlaps.argmax(axis=1)
  # Clamp labels for the background RoIs to 0
  labels[int(fg_rois_per_image):] = 0
  rois = all_rois[np.append(fg_inds, bg_inds)]
  bbox_target_data = helper(
    rois[:, 1:5], 
    gt_boxes[temp[np.append(fg_inds, bg_inds)], :4],
    labels)
  idx = np.where(bbox_target_data[:, 0] > 0)[0]
  tmp1 = np.zeros(np.zeros((bbox_target_data[:, 0].size, 4 * num_classes), dtype=np.float32).shape, dtype=np.float32)
  for ind in idx:
    cls = bbox_target_data[:, 0][ind]
    start = int(4 * cls)
    end = start + 4
    np.zeros((bbox_target_data[:, 0].size, 4 * num_classes), dtype=np.float32)[ind, start:end] = bbox_target_data[ind, 1:]
    tmp1[ind, start:end] = cfg.TRAIN.BBOX_INSIDE_WEIGHTS

  return labels, rois.reshape(-1, 5), all_scores[np.append(fg_inds, bg_inds)].reshape(-1), np.zeros((bbox_target_data[:, 0].size, 4 * num_classes), dtype=np.float32).reshape(-1, num_classes * 4), tmp1.reshape(-1, num_classes * 4)


def helper(ex_rois, gt_rois, labels):
  """Helper function to compute bounding-box regression targets for an image."""

  targets = bbox_transform(ex_rois, gt_rois)
  if cfg.TRAIN.BBOX_NORMALIZE_TARGETS_PRECOMPUTED:
    # Optionally normalize targets by a precomputed mean and stdev
    targets = ((targets - np.array(cfg.TRAIN.BBOX_NORMALIZE_MEANS))
               / np.array(cfg.TRAIN.BBOX_NORMALIZE_STDS))
  return np.hstack(
    (labels[:, np.newaxis], targets)).astype(np.float32, copy=False)


