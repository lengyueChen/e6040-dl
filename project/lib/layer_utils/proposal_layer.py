# --------------------------------------------------------
# Faster R-CNN
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick,Xinlei Chen and Yiran Shi
# --------------------------------------------------------
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import numpy as np
from model.config import cfg
from model.bbox_transform import bbox_transform_inv, clip_boxes, bbox_transform_inv_tf, clip_boxes_tf
from model.nms_wrapper import nms


def proposal_layer(rpn_cls_prob, rpn_bbox_pred, im_info, cfg_key, _feat_stride, anchors, num_anchors):
  """ Compute the scores for each bounding box 
  and get the coordiante of the bounding box
  """
  spe = (-1, 4)
  region_prop = clip_boxes_tf(bbox_transform_inv_tf(anchors, tf.reshape(rpn_bbox_pred, shape=spe)), im_info[:2])
  idx = tf.image.non_max_suppression(region_prop, tf.reshape(rpn_cls_prob[:, :, :, num_anchors:], shape=(-1,)), max_output_size=cfg[cfg_key].RPN_POST_NMS_TOP_N, iou_threshold=cfg[cfg_key].RPN_NMS_THRESH)
  boxes = tf.to_float(tf.gather(region_prop, idx))
  ret1 = tf.concat([tf.zeros((tf.shape(idx)[0], 1), dtype=tf.float32), boxes], 1)
  ret2 = tf.reshape(tf.gather(tf.reshape(rpn_cls_prob[:, :, :, num_anchors:], shape=(-1,)), idx), shape=(-1, 1))
  return ret1,ret2 
